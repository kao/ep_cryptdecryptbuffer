﻿/*
 *
 * Non-standard IDEA CBC mode implementation, as used in Enigma EP_CryptDecryptBuffer function
 * (c) kao <kao.was.here@gmail.com>, 2017.
 * 
 * Loosely based on https://github.com/LexBritvin/IdeaCipher/blob/master/IdeaCipher/Idea.cs
 *
 */

using System;
using System.Text;
using System.Security.Cryptography;

namespace IdeaCipher
{
    public class Idea
    {
        // Number of rounds.
        internal static int rounds = 8;
        // Internal encryption sub-keys.
        internal int[] subKeyEncr;
        internal int[] subKeyDecr;
        // IV for CBC mode
        byte[] IV;

        public Idea(String charKey)
        {
            byte[] key = generateUserKeyFromCharKey(charKey);
            // Expands a 16-byte user key to the internal encryption sub-keys.
            int[] tempSubKey = expandUserKey(key);

            subKeyEncr = tempSubKey;
            subKeyDecr = invertSubKey(tempSubKey);

            // calculate IV
            IV = new byte[8];
            crypt(IV, 0, true);
        }

        /**
         * Encrypts array
         *
         * @param data
         *    Buffer containing data bytes to be encrypted
         */
        public void Encrypt(byte[] data)
        {
            if (data.Length % 8 != 0)
            {
                throw new ArgumentException("Data length must be divisible by 8", "data");
            }
            for (int q = 0; q < data.Length; q += 8)
            {
                // xor data with previous IV
                for (int w = 0; w < 8; w++)
                   data[q + w] ^= IV[w];

                // encrypt data
                crypt(data, q, true);

                // save resulting bytes for next IV
                Array.Copy(data, q, IV, 0, 8);
            }

        }

        /**
         * Decrypts array
         *
         * @param data
         *    Buffer containing data bytes to be decrypted
         */
        public void Decrypt(byte[] data)
        {
            if (data.Length % 8 != 0)
            {
                throw new ArgumentException("Data length must be divisible by 8", "data");
            }
            for (int q = 0; q < data.Length; q += 8)
            {
                // save original bytes for next IV
                byte[] origBytes = new byte[8];
                Array.Copy(data, q, origBytes, 0, 8);

                // decrypt data
                crypt(data, q, false);

                // xor data with previous IV
                for (int w = 0; w < 8; w++)
                   data[q + w] ^= IV[w];

                // copy original data to next IV
                Array.Copy(origBytes, IV, 8);
            }
        }

        /**
         * Encrypts or decrypts a block of 8 data bytes.
         *
         * @param data
         *    Data buffer containing the bytes to be encrypted/decrypted.
         * @param dataPos
         *    Start position of the 8 bytes within the buffer.
         * @param encrypt
         *    true if encrypting data, false if decrypting
         */
        private void crypt(byte[] data, int dataPos, bool encrypt)
        {
            int[] subKey;
            if (encrypt)
               subKey = subKeyEncr;
            else
               subKey = subKeyDecr;

            int x0 = ((data[dataPos + 0] & 0xFF) << 8) | (data[dataPos + 1] & 0xFF);
            int x1 = ((data[dataPos + 2] & 0xFF) << 8) | (data[dataPos + 3] & 0xFF);
            int x2 = ((data[dataPos + 4] & 0xFF) << 8) | (data[dataPos + 5] & 0xFF);
            int x3 = ((data[dataPos + 6] & 0xFF) << 8) | (data[dataPos + 7] & 0xFF);
            //
            int p = 0;
            for (int round = 0; round < rounds; round++)
            {
                int y0 = mul(x0, subKey[p++]);
                int y1 = add(x1, subKey[p++]);
                int y2 = add(x2, subKey[p++]);
                int y3 = mul(x3, subKey[p++]);
                //
                int t0 = mul(y0 ^ y2, subKey[p++]);
                int t1 = add(y1 ^ y3, t0);
                int t2 = mul(t1, subKey[p++]);
                int t3 = add(t0, t2);
                //
                x0 = y0 ^ t2;
                x1 = y2 ^ t2;
                x2 = y1 ^ t3;
                x3 = y3 ^ t3;
            }
            //
            int r0 = mul(x0, subKey[p++]);
            int r1 = add(x2, subKey[p++]);
            int r2 = add(x1, subKey[p++]);
            int r3 = mul(x3, subKey[p++]);
            //
            data[dataPos + 0] = (byte)(r0 >> 8);
            data[dataPos + 1] = (byte)r0;
            data[dataPos + 2] = (byte)(r1 >> 8);
            data[dataPos + 3] = (byte)r1;
            data[dataPos + 4] = (byte)(r2 >> 8);
            data[dataPos + 5] = (byte)r2;
            data[dataPos + 6] = (byte)(r3 >> 8);
            data[dataPos + 7] = (byte)r3;
        }

        // Expands a 16-byte user key to the internal encryption sub-keys.
        private static int[] expandUserKey(byte[] userKey)
        {
            if (userKey.Length != 16)
            {
                throw new ArgumentException("Key length must be 128 bit", "key");
            }
            int[] key = new int[rounds * 6 + 4];
            for (int i = 0; i < userKey.Length / 2; i++)
            {
                key[i] = ((userKey[2 * i] & 0xFF) << 8) | (userKey[2 * i + 1] & 0xFF);
            }
            for (int i = userKey.Length / 2; i < key.Length; i++)
            {
                key[i] = ((key[(i + 1) % 8 != 0 ? i - 7 : i - 15] << 9) | (key[(i + 2) % 8 < 2 ? i - 14 : i - 6] >> 7)) & 0xFFFF;
            }
            return key;
        }

        // Inverts decryption/encrytion sub-keys to encrytion/decryption sub-keys.
        private static int[] invertSubKey(int[] key)
        {
            int[] invKey = new int[key.Length];
            int p = 0;
            int i = rounds * 6;
            invKey[i + 0] = mulInv(key[p++]);
            invKey[i + 1] = addInv(key[p++]);
            invKey[i + 2] = addInv(key[p++]);
            invKey[i + 3] = mulInv(key[p++]);
            for (int r = rounds - 1; r >= 0; r--)
            {
                i = r * 6;
                int m = r > 0 ? 2 : 1;
                int n = r > 0 ? 1 : 2;
                invKey[i + 4] = key[p++];
                invKey[i + 5] = key[p++];
                invKey[i + 0] = mulInv(key[p++]);
                invKey[i + m] = addInv(key[p++]);
                invKey[i + n] = addInv(key[p++]);
                invKey[i + 3] = mulInv(key[p++]);
            }
            return invKey;
        }

        // Addition in the additive group.
        // The arguments and the result are within the range 0 .. 0xFFFF.
        private static int add(int a, int b)
        {
            return (a + b) & 0xFFFF;
        }

        // Multiplication in the multiplicative group.
        // The arguments and the result are within the range 0 .. 0xFFFF.
        private static int mul(int a, int b)
        {
            long r = (long)a * b;
            if (r != 0)
            {
                return (int)(r % 0x10001) & 0xFFFF;
            }
            else
            {
                return (1 - a - b) & 0xFFFF;
            }
        }

        // Additive Inverse.
        // The argument and the result are within the range 0 .. 0xFFFF.
        private static int addInv(int x)
        {
            return (0x10000 - x) & 0xFFFF;
        }

        // Multiplicative inverse.
        // The argument and the result are within the range 0 .. 0xFFFF.
        // The following condition is met for all values of x: mul(x, mulInv(x)) == 1
        private static int mulInv(int x)
        {
            if (x <= 1)
            {
                return x;
            }
            int y = 0x10001;
            int t0 = 1;
            int t1 = 0;
            while (true)
            {
                t1 += y / x * t0;
                y %= x;
                if (y == 1)
                {
                    return 0x10001 - t1;
                }
                t0 += x / y * t1;
                x %= y;
                if (x == 1)
                {
                    return t0;
                }
            }
        }
        // Generates a 16-byte binary user key from a character string key.
        private static byte[] generateUserKeyFromCharKey(String charKey)
        {
            MD5 md5 = MD5.Create();
            return md5.ComputeHash(Encoding.ASCII.GetBytes(charKey));
        }
    }
}
